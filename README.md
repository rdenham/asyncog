<!-- These are examples of badges you might want to add to your README:
     please update the URLs accordingly

[![Built Status](https://api.cirrus-ci.com/github/<USER>/asyncog.svg?branch=main)](https://cirrus-ci.com/github/<USER>/asyncog)
[![ReadTheDocs](https://readthedocs.org/projects/asyncog/badge/?version=latest)](https://asyncog.readthedocs.io/en/stable/)
[![Coveralls](https://img.shields.io/coveralls/github/<USER>/asyncog/main.svg)](https://coveralls.io/r/<USER>/asyncog)
[![PyPI-Server](https://img.shields.io/pypi/v/asyncog.svg)](https://pypi.org/project/asyncog/)
[![Conda-Forge](https://img.shields.io/conda/vn/conda-forge/asyncog.svg)](https://anaconda.org/conda-forge/asyncog)
[![Monthly Downloads](https://pepy.tech/badge/asyncog/month)](https://pepy.tech/project/asyncog)
[![Twitter](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter)](https://twitter.com/asyncog)
-->

# asyncog

## Accessing COGs over http Asyncrhonously

The idea is that sometimes you want a small amount of raster data
from many different COGs, and so you are usually IO bound. You can
get things to run faster if you could read the data in asynchronously,
but it isn't obvious how to do this when using standard python approaches 
such as [`rasterio`](https://rasterio.readthedocs.io/en/stable/) or 
[`gdal`](https://gdal.org/api/python_bindings.html). 

There is one package, [`aiocogeo`](https://github.com/geospatial-jeff/aiocogeo),
which is great, but I had a couple of problems with it. Some cogs I
was accessng didn't seem to have the overviews distributed in the way that `aiocogeo`
expected, and it didn't handle BigTiff. 

It probably would have been better to create a pull request, but I struggled
understanding how it worked by reading the code; I needed to implement it myself
to work it out. 

I took parts of code from a number of packages, mostly
[`tifftools`](https://github.com/DigitalSlideArchive/tifftools), 
[`aiocogeo`](https://github.com/geospatial-jeff/aiocogeo) and 
[`COGdumper`](https://github.com/donnyv/COGDumper)


This package does less than [`aiocogeo`](https://github.com/geospatial-jeff/aiocogeo),
and I've simplified things by:

* only http(s) for now
* I read the first `os.environ.get('COG_INGESTED_BYTES_AT_OPEN', '16384')`  bytes of the COG in,
  and hope that's enough, but fetch the offsets and bytesizes as required
* use `tifftools`, by making a file like, memory object, so I don't have to 
  translate all the seeks into slices. 
* not (yet) doing masks



# Usage

All the functions that require access to the COG expect a session argument.


First step is to get the image information:

```python
import asyncio
import aiohttp
from asyncog.asyncog import read_tiff

async with aiohttp.ClientSession() as session:
    info = await read_tiff('https://fs.rsdatascience.dev/test-cog2.tif', session)
```

The info object has most of the stuff you need to know, see
[`tifftools.py`](https://github.com/DigitalSlideArchive/tifftools/blob/29627d54cbda980cb2c2f4ccad190b0bb6d77507/tifftools/tifftools.py#L34) for details.


Then, once you have the info, you can do a range request to get the data you want.


# Example

This example gets a block of data from a number of COGS hosted
on `https://sentinel-cogs.s3.us-west-2.amazonaws.com` and accessed
through STAC provided by 'https://earth-search.aws.element84.com/v1/'


```python
from pystac_client import Client
import asyncio
import aiohttp
from asyncog.asyncog import read_tiff, get_tile_data

URL = 'https://earth-search.aws.element84.com/v1/'
client = Client.open(URL)

search = client.search(
    max_items = 130,
    collections = "sentinel-2-l2a",
    bbox = (140.99377687624792,-26.0041705951394,141.01307200077315,-26.002614275967673)
)
urls = []
for item in search.item_collection():
    red_asset = item.assets['red']
    urls.append(red_asset.href)


# getting a particular tile
# for example
xyz = (6, 5, 0)

# note that the function `get_tile_data` expects
# an info object, so to take advantage of the 
# async nature, these should be grouped together
# otherwise you'd have to run all the requests
# to  get the info, before the requests to get
# the data

async def gd(xyz, url, session):
    info = await read_tiff(url, session)
    dat = await get_tile_data(*xyz, info, session)
    return dat


async def main():
    tasks = []
    async with aiohttp.ClientSession() as session:
        for url in urls:
            tasks.append(asyncio.create_task(gd(xyz, url, session)))
        data_list = await asyncio.gather(*tasks)
        return data_list

#%%time
alldata =  asyncio.run(main())
```


When I time the data part (excluding STAC search) I get:

```s
CPU times: user 2.43 s, sys: 215 ms, total: 2.65 s
Wall time: 5.75 s
```

But this seems very variable.


Comparing with `rasterio`:

```python
import rasterio as rio
from rasterio.windows  import Window
from pystac_client import Client

URL = 'https://earth-search.aws.element84.com/v1/'
client = Client.open(URL)

search = client.search(
    max_items = 130,
    collections = "sentinel-2-l2a",
    bbox = (140.99377687624792,-26.0041705951394,141.01307200077315,-26.002614275967673)
)
urls = []
for item in search.item_collection():
    red_asset = item.assets['red']
    urls.append(red_asset.href)


mywindow = Window(col_off = 6 * 1024,
      row_off = 5 * 1024,
      width=1024,
      height=1024)

alldata2 = []
# %%time
for url in urls:
    with rio.open(url) as src:
        r = src.read(1, window=mywindow)
        alldata2.append(r)

# check that the data is all the same
# if raster is 1 band, rasterio returns shape (ny, nx), but I return (1, ny, nx)
for i in range(len(urls)):
    assert np.alltrue(alldata[i][0] == alldata2[i])


```

That took 

``` 
CPU times: user 5.54 s, sys: 645 ms, total: 6.19 s
Wall time: 5min 28s
```


The above sort of assumes you know the tile you want to get. But
that doesn't normally happen. Let's now assume you have a 
point that you want to extract data from. So first find
the tile that the point lies in.


You can get the geo transformation via 
[`affine`](https://github.com/rasterio/affine)
and a little bit of division to get the necessary
tile.


```python
import aiohttp
import asyncio
from asyncog.asyncog import geotransform as agf
from pystac_client import Client
from asyncog.asyncog import read_tiff, get_tile_data
from asyncog.constants import geotiff_tag as Tag

URL = 'https://earth-search.aws.element84.com/v1/'
client = Client.open(URL)

search = client.search(
    max_items = 130,
    collections = "sentinel-2-l2a",
    bbox = (140.99377687624792,-26.0041705951394,141.01307200077315,-26.002614275967673)
)
urls = []
for item in search.item_collection():
    red_asset = item.assets['red']
    urls.append(red_asset.href)

async with aiohttp.ClientSession() as session:
    info = await read_tiff(urls[0], session)
    
gt = agf(info)
invgt = ~gt
# from this we can work out which block a point lies
x, y = (554880.000, 7145140.000)
col, row = invgt * (x, y)

# now work out which block this is in (given overview 0)
nodataval = info["ifds"][0]["tags"][int(Tag.GDAL_NoData)]["data"]
tilewidth = info['ifds'][0]['tags'][int(Tag.TILEWIDTH)]['data'][0]
tileheight = info['ifds'][0]['tags'][int(Tag.TILEHEIGHT)]['data'][0]

imagewidth = info['ifds'][0]['tags'][int(Tag.ImageWidth)]['data'][0]
imageheight = info['ifds'][0]['tags'][int(Tag.ImageHeight)]['data'][0]

xyz = (col//tilewidth, row//tileheight, 0)

# and the offset, so we can get the actual pixel value
(xoff, yoff) = (int(col % tilewidth), int(row % tileheight))

# and using the same approach as the first example

async def gd(x, y, url, session):
    info = await read_tiff(url, session)
    gt = agf(info)
    invgt = ~gt
    # from this we can work out which block a point lies
    col, row = invgt * (x, y)
    imagewidth = info['ifds'][0]['tags'][int(Tag.ImageWidth)]['data'][0]
    imageheight = info['ifds'][0]['tags'][int(Tag.ImageHeight)]['data'][0]
    nodataval = info["ifds"][0]["tags"][int(Tag.GDAL_NoData)]["data"]
    if col < 0 or col > imagewidth or row < 0 or row > imageheight:
        return nodataval
    tilewidth = info['ifds'][0]['tags'][int(Tag.TILEWIDTH)]['data'][0]
    tileheight = info['ifds'][0]['tags'][int(Tag.TILEHEIGHT)]['data'][0]
    xyz = (col//tilewidth, row//tileheight, 0)
    dat = await get_tile_data(*xyz, info, session)
    (xoff, yoff) = (int(col % tilewidth), int(row % tileheight))
    return dat[0, xoff, yoff]


async def main():
    tasks = []
    async with aiohttp.ClientSession() as session:
        for url in urls:
            tasks.append(asyncio.create_task(gd(x,y, url, session)))
        data_list = await asyncio.gather(*tasks)
        return data_list

#%%time
pixvals =  asyncio.run(main())


```


Takes about 6 seconds.

The same from rasterio:

```python
import rasterio as rio
from pystac_client import Client

URL = 'https://earth-search.aws.element84.com/v1/'
client = Client.open(URL)

search = client.search(
    max_items = 130,
    collections = "sentinel-2-l2a",
    bbox = (140.99377687624792,-26.0041705951394,141.01307200077315,-26.002614275967673)
)
urls = []
for item in search.item_collection():
    red_asset = item.assets['red']
    urls.append(red_asset.href)

pixvals2 = []
x, y = (554880.000, 7145140.000)
#%%time
for url in urls:
    with rio.open(url) as src:
        val = next(src.sample([(x,y)]))
        pixvals2.append(val[0])

```

Takes a lot longer




## Overviews

Accessing overviews works the same way, you just provide
`(x, y, z)` where `x` and `y` are the block column and block
row values given overview `z`. Here's an example:

```python

url = 'https://fs.rsdatascience.dev/test-cog_bigtiff.tif'
xyz = (1, 1, 1)
async def main():
    async with aiohttp.ClientSession() as session:
        tiffinfo = await read_tiff(url, session)
        out_numpy_data = await get_tile_data(*xyz, tiffinfo, session)
    return out_numpy_data
asdat = asyncio.run(main())
print(asdat.shape)

# we also need the info for later
async with aiohttp.ClientSession() as session:
    info = await read_tiff(url, session)


```

It can be awkward comparing this to other methods, but here is one way.

First get the overview you are interested in. In gdal terms, '-ovr 0' is the
first overview, which corresponds to us as `z=1`.

```sh
gdal_translate -ovr 0 https://fs.rsdatascience.dev/test-cog_bigtiff.tif aa.tif
```

Now open this, and get the block you are interested in


```python
from rasterio.windows  import Window
from asyncog.constants import geotiff_tag as Tag

tile_width = info['ifds'][xyz[2]]["tags"][int(Tag.TILEWIDTH)]["data"][0]
tile_height = info['ifds'][xyz[2]]["tags"][int(Tag.TILEHEIGHT)]["data"][0]


with rio.open('aa.tif') as src:
    profile = src.profile
    mywindow = Window(col_off = xyz[0] * tile_width ,
      row_off = xyz[1] * tile_height,
      width=tile_width,
      height=tile_height)
    riosdat = src.read(window=mywindow)

np.all( asdat[0,0:129,0:384] == riosdat[0])

```

Note that the blocks will be zero padded, so all blocks will be 512 x 512 (or
whatever the block size is for the given overview).



## Polygon Extraction

I wrote this mostly because I needed to extract data from polygons.

The approach I take is to work out whic blocks the polygon lies on, 
and get the data from each block. I use `rasterio.features.rasterize`
to convert the geometry to a raster, and then fill the raster in 
from the block data extracted from the COG.

This works at the overview layers too.


Here's an example



```python
from asyncog.polyextract import extract_from_polygon
from asyncog.asyncog import read_tiff
import fiona 
from shapely.geometry import shape
import numpy as np 
import aiohttp
import asyncio

async def main():
    url = "https://data.tern.org.au/rs/public/data/landsat/seasonal_fractional_cover/ground_cover/qld//lztmre_qld_m202206202208_dixa2_vinterim.tif"
    geom = fiona.open('/vsicurl/https://fs.rsdatascience.dev/test_cog2.fgb')
    g2 = shape(geom[0].geometry)
    async with aiohttp.ClientSession() as session:
        info = await read_tiff(url, session)
        mdata = await extract_from_polygon(g2, info, session, overview=0)
    return mdata

mdata = asyncio.run(main())
# "should be" value was  done using exactextractr in R
shouldbe = np.array([138.6446,119.2488,140.6540])
meanvals = np.mean(mdata, axis=(1,2), where = (mdata != 0))
assert np.allclose(meanvals, shouldbe, 0.1)
```

I've compared these results using the excellent 
[`exactextract`](https://github.com/isciences/exactextract)
tool. As they point out, some compromises tend to be made, so that
a pixel is either declard within or outside the vector.

I simply pass the `all_touched` argument to 
[`rasterio.features.rasterize`](https://rasterio.readthedocs.io/en/latest/api/rasterio.features.html#rasterio.features.rasterize).



## General Headers
Sometimes you need to access imagery that requires some credentials, 
like on [TERN](https://www.tern.org.au/)

```python

xyz = (300, 300, 0)
async def main():
    url =  'https://dap.tern.org.au/thredds/fileServer/landscapes/remote_sensing/sentinel2/seasonal_fractional_cover/fractional_cover/qld/cvmsre_qld_m202306202308_acaa2.tif'
    if not os.getenv('TERN_API'):
        raise ValueError('tern api key not defined')
    session_headers = {'X-Api-Key':os.getenv('TERN_API')}
    async with aiohttp.ClientSession(headers=session_headers) as session:
        out_numpy_data = await get_tile_data(*xyz, url, session)
    return out_numpy_data
dat = asyncio.run(main())

```

You can also use a proxy, see [aiohttp documentation](https://docs.aiohttp.org/en/stable/client_advanced.html#proxy-support) for details,
but often the simplest thing to do is to use

```python
aiohttp.ClientSession(headers=session_headers, trust_env=True)
```

in your call to the client session.

## S3 like Data

I think in most situations, you could use credentials to first generate a presigned url,
and then use `asyncog` as normal.  Say you have an image on S3 like
`s3://sentinel-cogs/sentinel-s2-l2a-cogs/54/J/VS/2024/2/S2A_54JVS_20240203_0_L2A/B04.tif`.
This is interpreted as `bucket=sentinel-cogs`, 
`key=sentinel-s2-l2a-cogs/54/J/VS/2024/2/S2A_54JVS_20240203_0_L2A/B04.tif`

You can get a url that works with aiohttp lke:

```python
import asyncio
import aiohttp
import boto3
from asyncog.asyncog import read_tiff, get_tile_data


s3_client = boto3.client(
        service_name='s3', 
        aws_access_key_id=YOUR_AWS_KEY_ID,
        aws_secret_access_key=YOUR_AWS_SECRET_KEY,
        region_name="us-west-2"
    )

# Generate the URL to get 'key-name' from 'bucket-name'
url = s3_client.generate_presigned_url(
    ClientMethod='get_object',
    Params={
        'Bucket':'sentinel-cogs',
        'Key':'sentinel-s2-l2a-cogs/54/J/VS/2024/2/S2A_54JVS_20240203_0_L2A/B04.tif'
    }
)

# now that we have it we can use normal asyncog
xyz = (6, 5, 0)

async def gd(xyz, url, session):
    info = await read_tiff(url, session)
    dat = await get_tile_data(*xyz, info, session)
    return dat

async with aiohttp.ClientSession() as session:
    egdata =  await gd(xyz, url, session)



```



## Caching

I'm not sure it's worth it or not, but since I'm using a session, you could consider
caching some of the get requests. In particular, it often happens that you are 
frequently reading the image metadata from the same url. It may make sense to cache that.
Since we are doing partial reads, you might want to allow code 206 in the cache.


```python
from aiohttp_client_cache import CachedSession, CacheBackend
cache = CacheBackend(allowed_codes=(200, 206, 418),
                      expire_after=timedelta(days=1),
                      include_headers=True)
session = CachedSession(cache=cache)       
```

Then use that `session` in your extraction code.

## Test Application

```sh
 time python egapp.py '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"coordinates":[[[147.0674357930995,-23.405420841978497],[146.94617515618154,-23.574224497804522],[147.17786958743727,-23.619863573841215],[147.0674357930995,-23.405420841978497]]],"type":"Polygon"}}]}'

```

```sh
real    0m16.990s
user    0m15.975s
sys     0m0.696s
```

Comparing with vegmachine

```sh
poly=%7B%22type%22%3A%22FeatureCollection%22%2C%22features%22%3A%5B%7B%22type%22%3A%22Feature%22%2C%22properties%22%3A%7B%7D%2C%22geometry%22%3A%7B%22coordinates%22%3A%5B%5B%5B147.0674357930995%2C-23.405420841978497%5D%2C%5B146.94617515618154%2C-23.574224497804522%5D%2C%5B147.17786958743727%2C-23.619863573841215%5D%2C%5B147.0674357930995%2C-23.405420841978497%5D%5D%5D%2C%22type%22%3A%22Polygon%22%7D%7D%5D%7D

time curl "https://apidev-vm.jrsrp.com/tsjson?geoJsonPoly=${poly}&wmsLayer=aus%3Aground_cover_v3&monthlyRainfall=yes" > api1test.txt

```

```sh
real    0m19.625s
user    0m0.110s
sys     0m0.021s
```

A little faster at overview 0

## Increasing/Decreasing min num tiles

The number of tiles you need has a big impact on the time. So if you have over 100 blocks to check,
it's going to take over a minute. If you have only 8, will take about 6 seconds. Might want to see
how this compares in terms of precision.

```sh
time python egapp.py --maxtiles=128 '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"coordinates":[[[149.4197529677724,-24.953761064439632],[148.48183003037332,-28.469860069520124],[152.33571158216972,-28.18912392102667],[149.4197529677724,-24.953761064439632]]],"type":"Polygon"}}]}' > res128.txt
# 54s

time python egapp.py --maxtiles=64 '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"coordinates":[[[149.4197529677724,-24.953761064439632],[148.48183003037332,-28.469860069520124],[152.33571158216972,-28.18912392102667],[149.4197529677724,-24.953761064439632]]],"type":"Polygon"}}]}' > res64.txt
# 20s

time python egapp.py --maxtiles=16 '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"coordinates":[[[149.4197529677724,-24.953761064439632],[148.48183003037332,-28.469860069520124],[152.33571158216972,-28.18912392102667],[149.4197529677724,-24.953761064439632]]],"type":"Polygon"}}]}' > res16.txt
# 11s

time python egapp.py --maxtiles=8 '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"coordinates":[[[149.4197529677724,-24.953761064439632],[148.48183003037332,-28.469860069520124],[152.33571158216972,-28.18912392102667],[149.4197529677724,-24.953761064439632]]],"type":"Polygon"}}]}' > res8.txt
# 7s

time python egapp.py --maxtiles=4 '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"coordinates":[[[149.4197529677724,-24.953761064439632],[148.48183003037332,-28.469860069520124],[152.33571158216972,-28.18912392102667],[149.4197529677724,-24.953761064439632]]],"type":"Polygon"}}]}' > res4.txt
# 6s

```
