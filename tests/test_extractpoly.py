import fiona
import aiohttp
from asyncog.polyextract import extract_from_polygon
from asyncog.asyncog import read_tiff, get_tile_data
from shapely.geometry import shape
from shapely import from_wkt
import numpy as np 
import asyncio 
import logging 
import rasterio as rio 
import pytest
import rasterio
from rasterio import mask

logging.basicConfig(level=logging.DEBUG)  # all other messages at warn level

def test_poly_extract():
    async def main():
        geom = fiona.open('/vsicurl/https://fs.rsdatascience.dev/test_cog.fgb')
        g2 = shape(geom[0].geometry)
        async with aiohttp.ClientSession() as session:
            url = 'https://fs.rsdatascience.dev/test-cog.tif'
            info = await read_tiff(url, session)
            mdata = await extract_from_polygon(g2, info, session, overview=0)
        return mdata

    mdata = asyncio.run(main())
    wegot = np.unique(mdata[mdata != 0])
    shouldbe = np.array([19,20,26,27])
    assert np.all(shouldbe == wegot), 'Incorrect values'


def test_poly_extract2():
    """ 
    Using real data
    """ 
    async def main():
        url = "https://data.tern.org.au/rs/public/data/landsat/seasonal_fractional_cover/ground_cover/qld//lztmre_qld_m202206202208_dixa2_vinterim.tif"
        geom = fiona.open('/vsicurl/https://fs.rsdatascience.dev/test_cog2.fgb')
        g2 = shape(geom[0].geometry)
        async with aiohttp.ClientSession() as session:
            info = await read_tiff(url, session)
            mdata = await extract_from_polygon(g2, info, session, overview=0)
        return mdata

    mdata = asyncio.run(main())
    # should be done using exactextractr in R
    shouldbe = np.array([138.6446,119.2488,140.6540])
    meanvals = np.mean(mdata, axis=(1,2), where = (mdata != 0))
    assert np.allclose(meanvals, shouldbe, 0.1)


def test_poly_extract3():
    """ 
    Using real data, over several tiles
    """ 
    async def main():
        url = "https://data.tern.org.au/rs/public/data/landsat/seasonal_fractional_cover/ground_cover/qld//lztmre_qld_m202206202208_dixa2_vinterim.tif"
        geom = fiona.open('/vsicurl/https://fs.rsdatascience.dev/test_diag.fgb')
        g2 = shape(geom[0].geometry)
        async with aiohttp.ClientSession() as session:
            info = await read_tiff(url, session)
            mdata = await extract_from_polygon(g2, info, session, overview=0)
        return mdata

    mdata = asyncio.run(main())
    # should be done using exactextractr in R
    shouldbe = np.array([106.8532,113.4857,178.275])
    meanvals = np.mean(mdata, axis=(1,2), where = (mdata != 0))
    assert np.allclose(meanvals, shouldbe, 0.1)


def test_poly_extract4():
    """ 
    large poly, with overview
    """ 
    async def main():
        url = "https://data.tern.org.au/rs/public/data/landsat/seasonal_fractional_cover/ground_cover/qld//lztmre_qld_m202206202208_dixa2_vinterim.tif"
        geom = fiona.open('/vsicurl/https://fs.rsdatascience.dev/test_largepoly.fgb')
        g2 = shape(geom[0].geometry)
        async with aiohttp.ClientSession() as session:
            info = await read_tiff(url, session)
            mdata = await extract_from_polygon(g2, info, session, overview=4)
        return mdata

    mdata = asyncio.run(main())
    # should be done using exactextractr in R
    # bit harder, need to get the overview first overview 4 for me
    # is -ovr 3
    # gdal_translate /vsicurl/https://data.tern.org.au/rs/public/data/landsat/seasonal_fractional_cover/ground_cover/qld//lztmre_qld_m202206202208_dixa2_vinterim.tif \
    #                -ovr 3 \
    #                -projwin 1447440.6400197159964591  -2545663.3897126796655357  1713422.7736549649853259   -2747845.6149815893732011 \
    #                sub.tif
    shouldbe = np.array([116.5776,  134.1793,  148.0938])
    meanvals = np.mean(mdata, axis=(1,2), where = (mdata != 0))
    assert np.allclose(meanvals, shouldbe, 0.1)


@pytest.mark.asyncio
async def test_partial_cover():
    geom_wkt= 'POLYGON ((704702.9573975108 7104461.68689051, 703919.1777316218 7102076.99556664, 705453.3847371871 7099708.980405875, 706337.2213816999 7097691.164670292, 709472.3400452498 7097657.812344084, 709989.3011014728 7100426.055419345, 710889.8139090867 7104044.78281291, 707421.171983458 7104661.800847758, 704702.9573975108 7104461.68689051))'
    geom = from_wkt(geom_wkt)
    url = 'https://fs.rsdatascience.dev/test-cog.tif'

    async with aiohttp.ClientSession() as session:
        info = await read_tiff(url, session)
        mdata = await extract_from_polygon(geom, info, session, overview=0, all_touched=False)

    with rasterio.open(url) as src:
        out_image, out_transform = mask.mask(src, [geom], crop=True)
    
    assert mdata[mdata==3].shape == out_image[out_image==3].shape
    assert mdata[mdata==4].shape == out_image[out_image==4].shape
    # and all_touched is true
    async with aiohttp.ClientSession() as session:
        info = await read_tiff(url, session)
        mdata = await extract_from_polygon(geom, info, session, overview=0, all_touched=True)

    with rasterio.open(url) as src:
        out_image, out_transform = mask.mask(src, [geom], crop=True, all_touched=True)
    assert mdata[mdata==3].shape == out_image[out_image==3].shape
    assert mdata[mdata==4].shape == out_image[out_image==4].shape
