from asyncog.asyncog import read_tiff 
from asyncog.constants import geotiff_tag as Tag 
from asyncog.asyncog import get_tile_data
import aiohttp 
import asyncio
from tifftools.commands import _tiff_dump_ifds as dump_ifds
import rasterio as rio
from rasterio.windows  import Window
import numpy as np

def test_basic():
    async def main():
        url = 'https://fs.rsdatascience.dev/test-cog.tif'
        session_headers = {}
        async with aiohttp.ClientSession(headers=session_headers) as session:
            tiffinfo = await read_tiff(url, session)
        return tiffinfo
    
    loop = asyncio.get_event_loop()
    tiffinfo = loop.run_until_complete(main())
    dump_ifds(tiffinfo['ifds'], 100)


def test_bigtiff():
    xyz = (0, 0, 0)
    url = 'https://fs.rsdatascience.dev/test_cog2_band_bigtiff.tif'
    async def main():
        async with aiohttp.ClientSession() as session:
            tiffinfo = await read_tiff(url, session)
            out_numpy_data = await get_tile_data(*xyz, tiffinfo, session)
        return out_numpy_data
    asdat = asyncio.run(main())
    
    assert np.all(np.unique(asdat[0]) == np.array([1,2,8,9]))
    # and compare with rios
    mywindow = Window(col_off = xyz[0] * 512,
      row_off = xyz[1] * 512,
      width=512,
      height=512)
    with rio.open(url) as src:
        riosdat = src.read(window=mywindow)
    assert np.alltrue(riosdat==asdat)


def test_overview():
    xyz = (0, 0, 1)
    url = 'https://fs.rsdatascience.dev/test_cog2_band_bigtiff.tif'
    async def main():
        async with aiohttp.ClientSession() as session:
            tiffinfo = await read_tiff(url, session)
            out_numpy_data = await get_tile_data(*xyz, tiffinfo, session)
        return out_numpy_data
    asdat = asyncio.run(main())
    # and compare with rios
    mywindow = Window(col_off = xyz[0] * 512 ,
      row_off = xyz[1] * 512,
      width=512 * (1 + xyz[2]),
      height=512* (1 + xyz[2]))
    with rio.open(url) as src:
        riosdat = src.read(window=mywindow, out_shape = (3, 512, 512))
    assert np.alltrue(riosdat==asdat)
    xyz = (0, 0, 2)
    asdat = asyncio.run(main())
    # I don't know exacty what rios does
    # but I checked with gdalstranslate, except for the 0 padding
    # this is ok


def test_cover():
    xyz = (212, 383, 0)
    url = 'https://dap.tern.org.au/thredds/fileServer/landscapes/remote_sensing/sentinel2/seasonal_fractional_cover/fractional_cover/qld/cvmsre_qld_m202103202105_acaa2.tif'
    async def main():
        async with aiohttp.ClientSession() as session:
            tiffinfo = await read_tiff(url, session)
            out_numpy_data = await get_tile_data(*xyz, tiffinfo, session)
        return out_numpy_data
    asdat = asyncio.run(main())
    blocksize = 256
    # and compare with rios
    mywindow = Window(col_off = xyz[0] * blocksize,
      row_off = xyz[1] * blocksize,
      width=blocksize,
      height=blocksize)
    with rio.open(url) as src:
        riosdat = src.read(window=mywindow)
    assert np.alltrue(riosdat==asdat)


def test_cover_over():
    xyz = (0, 0, 1)
    url = 'https://dap.tern.org.au/thredds/fileServer/landscapes/remote_sensing/sentinel2/seasonal_fractional_cover/fractional_cover/qld/cvmsre_qld_m202103202105_acaa2.tif'
    async def main():
        async with aiohttp.ClientSession() as session:
            tiffinfo = await read_tiff(url, session)
            out_numpy_data = await get_tile_data(*xyz, tiffinfo, session)
        return out_numpy_data
    asdat = asyncio.run(main())

