import imagecodecs
import io
import logging
import os
import struct
from math import ceil

import affine
import numpy as np
from tifftools import TifftoolsError
from tifftools.tifftools import read_ifd
from asyncog.exceptions import AsyncGOGError
from tifftools.constants import Datatype
from asyncog.constants import geotiff_tag as Tag
from aiohttp import ClientResponseError

logger = logging.getLogger(__name__)


async def read_tiff(url, session):
    """
    See the original at https://github.com/DigitalSlideArchive/tifftools

    Read the non-imaging data from a TIFF and return a Python structure with
    the results.  The path may optionally be terminated with
    [,<IFD #>[,[<SubIFD tag name or number>:]<SubIFD #>[,<IFD #>...]]
    If a component is not specified, all of the IFDs from that level are used,
    e.g., ",1" will read IFD 1 and all subifds, ",1,0" or ",1:SubIFD:0" will
    read the chain of IFDs in the first SubIFD record, ",1,0,2" will read IFD 2
    from IFD 1's first SubIFD.

    The output is an "info" dictionary containing the following keys:
    - ifds: a list of ifd records
    - path_or_fobj: the path of the file or a file-like object referencing the
        tiff file.
    - size: the total length of the tiff file in bytes.
    - header: the first four bytes of the tiff file
    - bigEndian: True if big endian, False if little endian
    - bigtiff: True if bigtiff, False if classic
    - endianPack: the byte-ordering-mark for struct.unpack (either '<' or '>')
    - firstifd: the offset of the first IFD in the file.
    Each IFD is a dictionary containing the following keys:
    - offset: the offset of this ifd.
    - path_or_fobj, size, bigEndian, bigtiff: copied from the file's info
        dictionary.
    - tagcount: number of tags in the IFD
    - tags: a dictionary of tags in this IFD.  The keys are the integer tag
        values.
    Each IFD tag is a dictionary containing the following keys:
    - datatype: the Datatype of the tag
    - count: the number of elements in the tag.  For most numeric values, this
        is the total number of entries.  For RATIONAL and SRATIONAL, this is
        the number of pairs of entries.  For ASCII, this is the length in bytes
        including a terminating null.  For UNDEFINED, this is the length in
        bytes.
    - datapos: the offset within the file (always within the IFD) that the data
        or offset to the data is located.
    - [offset]: if the count is large enough that the data cannot be stored in
        the IFD, this is the offset within the file of the data associated with
        the tag.
    - [ifds]: if the tag contains sub-ifds, this is a list of lists of IFDs.

    :param path: the file or stream to read.
    :returns: a dictionary of information on the tiff file.
    """
    info = {
        "ifds": [],
    }
    info["path_or_fobj"] = url
    logger.info(f"extracting info from {url}")

    # first get the header, we'll read as far as we can
    # to reduce the number of unncessary reads
    # start with 16384
    buff_size = int(os.environ.get("COG_INGESTED_BYTES_AT_OPEN", "16384"))
    logger.debug(f"{buff_size=}")
    range_headers = {"Range": f"bytes=0-{buff_size}"}
    # pretend we have a tiff like file object
    async with session.get(url, headers=range_headers) as response:
        try:
            response.raise_for_status()
            data = await response.content.read()
            tiff = io.BytesIO(data)
        except ClientResponseError as e:
            logger.error(e)
            raise e 
    tiff.seek(0, os.SEEK_END)
    info["size"] = tiff.tell()
    logger.debug(f"size of mem tiff is {info['size']}")
    tiff.seek(0)
    header = tiff.read(4)
    info["header"] = header
    if header not in (b"II\x2a\x00", b"MM\x00\x2a", b"II\x2b\x00", b"MM\x00\x2b"):
        raise TifftoolsError("Not a known tiff header for %s" % url)
    info["bigEndian"] = header[:2] == b"MM"
    info["endianPack"] = bom = ">" if info["bigEndian"] else "<"
    info["bigtiff"] = b"\x2b" in header[2:4]
    if info["bigtiff"]:
        offsetsize, zero, nextifd = struct.unpack(bom + "HHQ", tiff.read(12))
        if offsetsize != 8 or zero != 0:
            raise TifftoolsError("Unexpected offset size")
    else:
        nextifd = struct.unpack(bom + "L", tiff.read(4))[0]
    info["firstifd"] = nextifd
    while nextifd:
        nextifd = read_ifd(tiff, info, nextifd, info["ifds"])
    #logger.debug("read_tiff: %s", info)
    # explicitly get things that don't take up much space but we 
    # might have missed
    # like datasizes
    important_tagids = (int(Tag.get(name)) for name in ("SampleFormat", "ModelPixelScaleTag", "ModelTiepointTag"))
  
    for tagid in important_tagids:
        if not 'data' in info['ifds'][0]['tags'][tagid]:
            bom = info['endianPack']
            taginfo =info['ifds'][0]['tags'][tagid]
            typesize = Datatype[taginfo['datatype']].size
            start_pos = taginfo.get('offset', taginfo['datapos'])
            if start_pos < 0:
                raise ValueError("offset is negative: check your request")
            end_pos = start_pos + taginfo['count'] * typesize - 1
            range_headers = {"Range": f"bytes={start_pos}-{end_pos}"}
            async with session.get(url, headers=range_headers) as response:
                rawdata = await response.content.read()
            data = list(struct.unpack(
                bom + Datatype[taginfo['datatype']].pack * taginfo['count'], rawdata))
            info['ifds'][0]['tags'][tagid]['data'] = data

    # I'm having trouble with nodata actually
    # see https://github.com/geospatial-jeff/aiocogeo/issues/117
    tagid = int(Tag.get('GDAL_NoData'))
    taginfo =info['ifds'][0]['tags'][tagid]
    typesize = Datatype[taginfo['datatype']].size
    start_pos = taginfo.get('offset', taginfo['datapos'])
    if start_pos < 0:
        raise ValueError("offset is negative: check your request")
    end_pos = start_pos + taginfo['count'] * typesize - 1
    range_headers = {"Range": f"bytes={start_pos}-{end_pos}"}
    async with session.get(url, headers=range_headers) as response:
        rawdata = await response.content.read()
    value = int(rawdata.strip(b'\x00'))
    info['ifds'][0]['tags'][tagid]['data'] = value

    return info


def bands_in_separate_blocks(info, z):
    """ 
    check to see whether bands are separate or not
    """
    ifd = info["ifds"][z]
    image_width = ifd["tags"][int(Tag.IMAGEWIDTH)]["data"][0]
    image_height = ifd["tags"][int(Tag.IMAGEHEIGHT)]["data"][0]
    nbands = ifd["tags"][int(Tag.get("SamplesPerPixel"))]["data"][0]
    tile_width = ifd["tags"][int(Tag.TILEWIDTH)]["data"][0]
    nx_tiles = ceil(image_width / float(tile_width))
    tile_height = ifd["tags"][int(Tag.TILEHEIGHT)]["data"][0]
    ny_tiles = ceil(image_height / float(tile_height))
    total_tiles = ifd['tags'][int(Tag.TileOffsets)]['count']
    expected_tiles = nx_tiles * ny_tiles * nbands 
    logger.debug(f"{image_width=} {tile_width=}")
    logger.debug(f"{image_height=} {tile_height=}")
    logger.debug(f"{ny_tiles=} {nx_tiles=} {total_tiles=} {expected_tiles=}")
    if total_tiles < expected_tiles:
        # bands are in the tile
        # no multiplier
        return False
    else:
        return True 


async def get_range(x, y, z, info, session):
    """
    Given an x,y,z ifd info object, and the tag,
    work out the range required to fetch the data
    returns start, end, offfsets (adjusted to start from 0), 
    and the bytes in each chunk)

    seems to me that sometimes bands are in separate blocks, sometimes not
    and how you deal with it depends on this.

    I check

    """
    assert x >= 0, 'x must be positive'
    assert y >= 0, 'y must be positive'
    assert z >= 0, 'z must be positive' 
    # for the overall image, get the 0 info
    ifd = info["ifds"][z]
    image_width = ifd["tags"][int(Tag.IMAGEWIDTH)]["data"][0]
    image_height = ifd["tags"][int(Tag.IMAGEHEIGHT)]["data"][0]
    nbands = ifd["tags"][int(Tag.get("SamplesPerPixel"))]["data"][0]
    # now get the ifd for the overview that we want

    # idx is the tile id, but our bytecounts and our
    # offsets are for each group of bands
    tile_height = ifd["tags"][int(Tag.TILEHEIGHT)]["data"][0]
    ny_tiles = ceil(image_height / float(tile_height))

    tile_width = ifd["tags"][int(Tag.TILEWIDTH)]["data"][0]
    nx_tiles = ceil(image_width / float(tile_width))
    
    idx = (y * nx_tiles) + x
    logger.debug(f"{x=}, {y=}, {z=}, {ny_tiles=} {idx=}")

    # would be better to task/gather here, but there aren't many to call
    if bands_in_separate_blocks(info, 0):
        offsets = []
        bytecounts = []
        #then have to iterate over, they aren't consecutive
        for i  in range(nbands):
            thisidx = idx + nx_tiles * ny_tiles * i 
            thisoffset = await get_tag_data(thisidx, z, info, int(Tag.TileOffsets), session)
            offsets += thisoffset 
            thisbytecount = await get_tag_data(thisidx, z, info, int(Tag.TileByteCounts), session)
            bytecounts += thisbytecount 
    else:
        offsets = await get_tag_data(idx, z, info, int(Tag.TileOffsets), session)
        bytecounts = await get_tag_data(idx, z, info, int(Tag.TileByteCounts), session)
    logger.debug(f"{offsets=}")
    logger.debug(f"{bytecounts=}")
    return (offsets, bytecounts)


async def get_data(start_pos, end_pos, url, session):
    """
    Get the data from an image starting at byte start_pos
    and ending at byte end_pos
    """
    if start_pos < 0:
        raise ValueError("offset is negative: check your request")
    range_headers = {"Range": f"bytes={start_pos}-{end_pos}"}
    logger.debug(f"header={range_headers}")
    async with session.get(url, headers=range_headers) as response:
        data = await response.content.read()
    return data


def get_shape(x, y, z, info):
    """
    Get the shape of the output tile

    I think this is the nominal shape, array will be zero filled
    if you extend outside the edge

    # you should take into account x, y to work out what the shape _should_ be
    """
    ifd = info["ifds"][z]
    tile_height = ifd["tags"][int(Tag.TILEHEIGHT)]["data"][0]
    tile_width = ifd["tags"][int(Tag.TILEWIDTH)]["data"][0]
    bands = ifd["tags"][int(Tag.get("SamplesPerPixel"))]["data"][0]
    shape = (bands,  tile_height, tile_width)
    return shape


def get_datatype(info):
    """
    Get the data type
    """
    ifd = info["ifds"][0]
    bitspersample = ifd["tags"][int(Tag.get("BitsPerSample"))]["data"][0]
    sampleformat = ifd["tags"][int(Tag.get("SampleFormat"))]["data"][0]
    # pretty basic,
    # https://awaresystems.be/imaging/tiff/tifftags/sampleformat.html

    # 1 = unsigned integer data
    # 2 = two's complement signed integer data
    # 3 = IEEE floating point data
    # 4 = undefined data format
    if sampleformat == 1:
        dname = "uint"
    elif sampleformat == 2:
        dname = "int"
    elif sampleformat == 3:
        dname = "float"
    else:
        raise TypeError("undefined data type")
    dtype = np.dtype(f"{dname}{bitspersample}")
    return dtype


def geotransform(tiffinfo,  ovr_level: int = 0) -> affine.Affine:
    """
    Return the geotransform of the image at a specific 
    overview level (defaults to native resolution)
    """
    # the 0'th overerview with the bits we need
    ifd = tiffinfo["ifds"][0]

    # affine takes a,b,c,d,e,f
    scalevals = ifd["tags"][int(Tag.get("ModelPixelScaleTag"))]["data"]
    tiepointvals = ifd["tags"][int(Tag.get("ModelTiepointTag"))]["data"]
    gt = affine.Affine(
        scalevals[0], 0.0, tiepointvals[3], 0.0, -scalevals[1], tiepointvals[4]
    )
    # Decimate the geotransform if an overview is requested
    if ovr_level > 0:
        # get the bounds from the gt above
        tlx = gt.c
        tly = gt.f

        # the original image width values
        imagewidth = ifd["tags"][int(Tag.get("ImageWidth"))]["data"][0]
        imageheight = ifd["tags"][int(Tag.get("ImageHeight"))]["data"][0]
        brx = tlx + (gt.a * imagewidth)
        bry = tly + (gt.e * imageheight)
        bounds = (tlx, bry, brx, tly)
        # now those at the given overview
        ifd = tiffinfo["ifds"][ovr_level]
        imagewidth = ifd["tags"][int(Tag.get("ImageWidth"))]["data"][0]
        imageheight = ifd["tags"][int(Tag.get("ImageHeight"))]["data"][0]

        gt = affine.Affine.translation(bounds[0], bounds[3]) * affine.Affine.scale(
            (bounds[2] - bounds[0]) / imagewidth,
            (bounds[1] - bounds[3]) / imageheight,
        )
    return gt


async def get_tile_data(x, y, z, info, session):
    offsets, bytecounts = await get_range(x, y, z, info, session)
    logger.debug(f"{offsets=}, {bytecounts=}")

    out_shape = get_shape(x, y, z, info) 
    out_type = get_datatype(info)
    logger.debug(f"{out_shape=}")
    logger.debug(f"{out_type=}")
    compression = info['ifds'][0]['tags'][int(Tag.get("Compression"))]['data'][0]
    if compression == 8:
        decoder = imagecodecs.zlib_decode
        logger.debug("compression is zlib")
    elif compression == 5:
        decoder = imagecodecs.lzw_decode
        logger.debug("compression is lzw")
    else:
        raise ValueError(f"unhandled compression type ({compression})")
    # again, use task/gather here
    data_chunks = []
    url = info['path_or_fobj']
    for offset, nbytes in zip(offsets, bytecounts):
        end_pos = offset + nbytes 
        dat = await get_data(offset, end_pos, url, session)
        out_data = decoder(dat)
        out_numpy_data = np.frombuffer(out_data, out_type) 
        data_chunks.append(out_numpy_data)
    if len(data_chunks) > 1:
        out_numpy_data = np.stack(data_chunks)
    # whether or not we have to roll axis depends on whether
    # bands are separate chunks or not
    if bands_in_separate_blocks(info, z):
        # easy, just use the out_shape
        out_numpy_data.shape = out_shape
    else:
        # otherwise, we need to roll axis andstuff
        out_numpy_data.shape = (out_shape[1], out_shape[2], out_shape[0])
        out_numpy_data = np.rollaxis(out_numpy_data, 2, 0)
    predictor = info['ifds'][0]['tags'][int(Tag.get('Predictor'))]['data'][0]
    if predictor == 2:
        logger.debug("applying delta_decode")
        imagecodecs.delta_decode(out_numpy_data, out=out_numpy_data, axis=-1)   
    return out_numpy_data


async def get_tag_data(idxoffset, z, info, tag, session, tagset=Tag):
    """ 
    When you don't have the offsets, or the bytesize
    because the header is too big, you might
    want to fetch just what you need
    """
    bom = info['endianPack']
    ifd = info['ifds'][z]
    taginfo = ifd["tags"][tag]
    typesize = Datatype[taginfo['datatype']].size
    start_pos = int(taginfo.get('offset', taginfo['datapos']) + idxoffset * typesize)
    end_pos = start_pos + typesize - 1
    range_headers = {"Range": f"bytes={start_pos}-{end_pos}"}
    logger.debug(f"range header is {range_headers}")
    url = info['path_or_fobj']
    async with session.get(url, headers=range_headers) as response:
        rawdata = await response.content.read()
    # assume we're only doing this for tile offsets and byte sizes
    # but maybe that's a bad assumption?
    assert Datatype[taginfo['datatype']].pack is not None
    # i'm confused about datatype
    if 'uint64' in Datatype[taginfo['datatype']].desc.lower():
        outtype = 'uint64'
    elif 'uint32' in Datatype[taginfo['datatype']].desc.lower():
        outtype = 'uint32'
    else:
        raise ValueError(f"I don't know how to convert {Datatype[taginfo['datatype']].desc}")
    data = np.frombuffer(rawdata, outtype).tolist()
    return data 


