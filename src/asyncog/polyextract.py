from asyncog.asyncog import get_tile_data, get_datatype
from asyncog.asyncog import geotransform as agf
from asyncog.constants import geotiff_tag as Tag
import rasterio.features as riof
from affine import Affine
from math import ceil
import asyncio
import numpy as np 
import logging

logger = logging.getLogger(__name__)


def nudge_bounds(bounds, destaffine, blocksize):
  """ 
  take a bounds tuple, and bump it up so that it aligns
  with the block structure from the destaffine

  Assume we can round t 3 decimal places
  """
  # modified origin
  # shift left so it lies on a block boundary

  x0 = destaffine.c # parent
  x1 = bounds[0]
  nbfromx0 = ((x1 - x0)/round(destaffine.a, 3))// blocksize
  x1prime = x0 + nbfromx0 * blocksize * destaffine.a
  nblockwide = ceil(  ((bounds[2] - x1prime) / round(destaffine.a, 3))/blocksize )
  x2prime = x1prime + nblockwide * blocksize * round(destaffine.a, 3)

  # am I doing this right? 
  # shift up  so it lies on a block boundary
  y0 = destaffine.f 
  y1 = bounds[3]
  nblocksfromy0 = ( (y1 - y0)/round(destaffine.e, 3)) // blocksize
  # note we're starting from the bottom, so we add to y0
  y1prime = y0 + nblocksfromy0 * blocksize * round(destaffine.e, 3)
  nblockshigh = ceil( ((bounds[1] -y1prime)/round(destaffine.e, 3))/blocksize )
  y2prime = y1prime + nblockshigh * blocksize * round(destaffine.e, 3)

  newbounds = (x1prime, y2prime, x2prime, y1prime)
  return newbounds 


def prepare_polygon(geom, info, overview=0, all_touched=True):
    """ 
    To facilitate extracting polygon data from many
    aligned rasters we get the basic information from an info object
    first
    """ 

    nbands = info['ifds'][0]["tags"][int(Tag.get("SamplesPerPixel"))]["data"][0]
    blocksize = info['ifds'][overview]["tags"][int(Tag.TILEWIDTH)]["data"][0]
    nodataval = info['ifds'][0]["tags"][int(Tag.GDAL_NoData)]["data"]

    logger.debug(f" using fill value {nodataval}")

    gt = agf(info, overview)
    invgt = ~gt
    newbounds = nudge_bounds(geom.bounds, gt, blocksize)
    logger.debug(f"{newbounds=}")
    # the dimension in pixels:

    # the dimension in pixels:
    # this should be exactly number of blocks we found
    # so do the blocks first
    # without rounding first
    ncol = (newbounds[2] - newbounds[0])/gt.a
    nxblocks = round(ncol / blocksize)
    nrow = -(newbounds[3] - newbounds[1])/gt.e
    nyblocks = round(nrow / blocksize)

    # now fix the ncol, nrow
    ncol = nxblocks * blocksize
    nrow = nyblocks * blocksize
    logger.debug(f"found {nxblocks=}, {nyblocks=}")

    # get the location of the top left block in block coordinates
    col0, row0 = invgt * (newbounds[0], newbounds[3])

    # we now create 2 rasters, one at the block level, one at the raster level
    gt_block_level = Affine(
        a=round(gt.a, 3)*blocksize,
        b=0,
        c=newbounds[0],
        d=0,
        e=round(gt.e, 3)*blocksize,
        f=newbounds[3]
        )

    gt_pixel_level = Affine(
        a=round(gt.a, 3),
        b=0,
        c=newbounds[0],
        d=0,
        e=round(gt.e, 3),
        f=newbounds[3]
        )
    
    # a raster at the block level
    # always use all-touched here
    block_raster = riof.rasterize([geom,],
        transform=gt_block_level,
        out_shape=(nyblocks, nxblocks),
        all_touched=True
        )

    pixel_raster = riof.rasterize([geom,],
        transform=gt_pixel_level,
        out_shape=(nrow, ncol),
        all_touched=all_touched
        )
    return (block_raster, pixel_raster, (newbounds[0], newbounds[3]))


    

async def extract_from_polygon(geom, info, session, overview=0, all_touched=True):
    """ 
    Given a geometry polygon, and info for a cog,
    extract the pixels at overview (default is at
    full res)

    Assumes geom is in correct CRS
    Assume tilewdith = tileheight = blocksize

    rasterized values are filled with nodata value taken from the image

    """
    nbands = info['ifds'][0]["tags"][int(Tag.get("SamplesPerPixel"))]["data"][0]
    nodataval = info['ifds'][0]["tags"][int(Tag.GDAL_NoData)]["data"]

    # now work on the actual overview requested
    ifd = info['ifds'][overview]

    # all our blocksizes are same dimention x, and y, but maybe this
    # is a  poor assumption?
    blocksize = ifd["tags"][int(Tag.TILEWIDTH)]["data"][0]
    image_width = ifd["tags"][int(Tag.IMAGEWIDTH)]["data"][0]
    image_height = ifd["tags"][int(Tag.IMAGEHEIGHT)]["data"][0]
    # this refers to the image as a whole
    n_image_x_tiles = ceil(image_width / float(blocksize))
    n_image_y_tiles = ceil(image_height / float(blocksize))

    logger.debug(f" using fill value {nodataval}")

    gt = agf(info, overview)
    invgt = ~gt
    newbounds = nudge_bounds(geom.bounds, gt, blocksize)
    logger.debug(f"{newbounds=}")
    # the dimension in pixels:
    # this should be exactly number of blocks we found
    # so do the blocks first
    # without rounding first
    # these refer to the blocks in our sub raster
    ncol = (newbounds[2] - newbounds[0])/gt.a
    nxblocks = round(ncol / blocksize)
    nrow = -(newbounds[3] - newbounds[1])/gt.e
    nyblocks = round(nrow / blocksize)

    # now fix the ncol, nrow
    ncol = nxblocks * blocksize
    nrow = nyblocks * blocksize

    logger.debug(f"found {nxblocks=}, {nyblocks=}")

    # get the location of the top left block in block coordinates
    # we use the original bounds to avoid drifting outside
    # the buffered region
    oldbounds = geom.bounds
    col0, row0 = invgt * (oldbounds[0], oldbounds[3])

    # we now create 2 rasters, one at the block level, one at the raster level
    gt_block_level = Affine(
        a=round(gt.a, 3)*blocksize,
        b=0,
        c=newbounds[0],
        d=0,
        e=round(gt.e, 3)*blocksize,
        f=newbounds[3]
        )

    gt_pixel_level = Affine(
        a=round(gt.a, 3),
        b=0,
        c=newbounds[0],
        d=0,
        e=round(gt.e, 3),
        f=newbounds[3]
        )
    
    # a raster at the block level
    # always use all-touched here
    block_raster = riof.rasterize([geom,],
        transform=gt_block_level,
        out_shape=(nyblocks, nxblocks),
        all_touched=True
        )

    pixel_raster = riof.rasterize([geom,],
        transform=gt_pixel_level,
        out_shape=(nrow, ncol),
        all_touched=all_touched
        )
    
    # now loop through and get the block data
    xyz = (int(col0//blocksize), int(row0//blocksize), overview)
    logger.debug(f"will extract data from {nxblocks=} x  {nyblocks=} blocks")
    logger.debug(f"starting from {xyz=}")
    tasks = []
    data_j = []
    data_i = []
    for j in range(nxblocks):
        for i in range(nyblocks):
            if block_raster[i,j] == 1:
                # then we have some data
                thisxyz = (xyz[0] + j, xyz[1] + i, overview)
                if thisxyz[0] >= 0 and thisxyz[1] >= 0 and thisxyz[0] < n_image_x_tiles and thisxyz[1] < n_image_y_tiles:
                    tasks.append(asyncio.create_task(get_tile_data(*thisxyz, info, session)))
                    data_j.append(j)
                    data_i.append(i)
                    logger.debug(f"{thisxyz=} {n_image_x_tiles=}, {n_image_y_tiles=}")

    logger.info("waiting on extraction")
    alldata = await asyncio.gather(*tasks)
    logger.info("done")
    
    # now insert that data into our output array
    outtype = get_datatype(info)
    out = nodataval + np.zeros( (nbands, nrow, ncol), dtype=outtype)

    counter = 0
    for counter, (j, i) in enumerate(zip(data_j, data_i)):
        startx = blocksize * j
        starty = blocksize * i 
        out[:,starty:(starty + blocksize), startx:(startx + blocksize)] = \
            np.where(pixel_raster[starty:(starty + blocksize), startx:(startx + blocksize)] == 1, 
            alldata[counter], nodataval)
    
    # we now clip back to the original extent, i remove padding on the edges
    vcols = np.argwhere(np.any(pixel_raster, axis=0)).flatten().tolist()
    vrows = np.argwhere(np.any(pixel_raster, axis=1)).flatten().tolist()
    out = out[:, vrows[0]:(vrows[-1] + 1), vcols[0]:(vcols[-1]+1)]
    return out 
