""" 
Example application, using caching for info
""" 

import requests
from bs4 import BeautifulSoup
import shapely
import pyproj
import json
from shapely.ops import transform
import asyncio
from aiohttp_client_cache import CachedSession, SQLiteBackend, CacheBackend
from asyncog.polyextract import prepare_polygon, extract_blocks
from asyncog.constants import geotiff_tag as Tag
from asyncog.asyncog import read_tiff
import numpy as np
from datetime import timedelta
import logging 
import click 
import aiohttp


from asyncog.polyextract import extract_from_polygon

logging.basicConfig(level=logging.ERROR) # all other messages at warn level
logger = logging.getLogger('mod.app')
logger.setLevel(logging.INFO) # our messages at debug for this module


def geojson2shp(geojson):
    if geojson['type'] == 'Feature':
        return geojson2shp(geojson["geometry"])
    elif geojson['type'] == 'FeatureCollection':
        geometries = [geojson2shp(geom) for geom in geojson['features']]
        return shapely.geometry.GeometryCollection(geometries)
    elif geojson['type'] == 'Polygon':
        return shapely.geometry.Polygon(geojson['coordinates'][0])
    elif geojson['type'] == 'MultiPolygon':
        polygons = [
            geojson2shp(dict(type='Polygon', coordinates=poly))
            for poly in geojson['coordinates']
            ]
        return shapely.geometry.MultiPolygon(polygons)
    elif geojson['type'] == 'GeometryCollection':
        geometries = [geojson2shp(geom) for geom in geojson['geometries']]
        return shapely.geometry.GeometryCollection(geometries)
    else:
        raise Exception('Unknown geojson type: %s' % geojson)


def get_urls(mainurl):
    """ 
    get all the ground cvoer urls
    """
    page = requests.get(mainurl)

    soup = BeautifulSoup(page.content, "html.parser")
    allrefs = soup.find_all("a")
    fullurls = []
    for ref in allrefs:
        link = ref["href"]
        if link.endswith('.tif'):
            fullurl = f"{mainurl}/{link}"
            fullurls.append(fullurl)
    return fullurls


async def extract(url, geom, overview, session):
    info = await read_tiff(url, session)
    nodataval = info['ifds'][0]["tags"][int(Tag.GDAL_NoData)]["data"]
    logger.debug(f"checking {info['path_or_fobj']}")
    res = await extract_from_polygon(geom, info, session, overview=overview)
    meanvals = np.mean(res, axis=(1,2), where=(res != nodataval))
    logger.debug(f"got data from  {info['path_or_fobj']}")
    return meanvals


async def extract_all(urls, geom, overview, session):
    tasks = []
    for url in urls:
        tasks.append(asyncio.create_task(extract(url, geom, overview, session)))
    alldata = await asyncio.gather(*tasks)
    return alldata


async def asyncmain(urls, geom, maxtiles, cache):
    async with CachedSession(cache=cache)  as session:
    #async with aiohttp.ClientSession() as session:
        info = await read_tiff(urls[0], session)
        # lets do this until we get the right number of tiles
        # we'll work backwards
        noverviews = len(info['ifds'])
        seloverview = 0
        for ovr in range(noverviews-1, -1, -1):
            blockr, pixelr, origin = prepare_polygon(geom, info, overview=ovr)
            logger.debug(f"{ovr=} has {blockr.sum()} tiles")
            if blockr.sum() > maxtiles:
                seloverview = ovr + 1
                break
            ntiles = blockr.sum()
        logger.info(f"selected overview is {seloverview} with {ntiles=}")
        alldata = await extract_all(urls, geom, seloverview, session)
        for res in alldata:
            print(",".join((str(i) for i in res)))
    
    

def header_filter(response):
    """
    If the range didn't start from 0, then don't cache it
    Or if no range was given, then cache
    """ 
    range_request = response.request_info.headers.get('Range', 'bytes=0-')
    return range_request.startswith('bytes=0-')



@click.command()
@click.argument("geojsonpoly")
@click.option("--maxtiles", default=256, help="max tiles we want to extract")
def extract_groundcover(geojsonpoly, maxtiles):
    s = geojson2shp(json.loads(geojsonpoly))
    project = pyproj.Transformer.from_proj(
        pyproj.Proj(init='epsg:4326'), # source coordinate system
        pyproj.Proj(init='epsg:3577')) # australian albers
    g2 = transform(project.transform, s)  # apply projection

    urls = get_urls("https://data.tern.org.au/rs/public/data/landsat/seasonal_fractional_cover/ground_cover/qld/")

    # we'll work out the overview from the first sample
    cache2 = CacheBackend(allowed_codes=(200, 206, 418),
                      include_headers=True,
                      filter_fn=header_filter)
    asyncio.run(asyncmain(urls, g2, maxtiles, cache2))
    


if __name__ == "__main__":
    extract_groundcover()

